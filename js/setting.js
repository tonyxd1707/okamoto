jQuery(document).ready(function ($) {
	$('#button-click').click(function () {
		let object = $('div');
		for (let i = 0; i < object.length; i++) {
			if ($(object[i]).hasClass('red')) {
				$(object[i]).removeClass('red').addClass('yellow');
			}
			else if ($(object[i]).hasClass('yellow')) {
				$(object[i]).removeClass('yellow').addClass('red');
			}
		}
	});
});