var gulp = require('gulp');
var plumber = require('gulp-plumber');
var rename = require('gulp-rename');
var ejs = require("gulp-ejs");
var sass = require("gulp-sass");
var sourcemaps = require('gulp-sourcemaps');
var minifyCss = require('gulp-minify-css');
var autoprefixer = require('gulp-autoprefixer');
var fs = require('fs');

gulp.task('ejs', function () {
	var tmp_file = './ejs/template.ejs';
	var json_file = './ejs/data/pages.json';
	var json = JSON.parse(fs.readFileSync(json_file));
	var page_data = json.pages;
	for (var i = 0; i < page_data.length; i++) {
		var id = page_data[i].id;
		var parentId = page_data[i].parentId;
		var depth = page_data[i].depth;
		var RELATIVE_PATH = "";
		if (depth == 0) {
			RELATIVE_PATH = "./"
		} else if (depth == 1) {
			RELATIVE_PATH = "../"
		} else if (depth == 2) {
			RELATIVE_PATH = "../../"
		}
		gulp.src(tmp_file)
			.pipe(plumber())
			.pipe(ejs({
				pageData: page_data[i],
				RELATIVE_PATH: RELATIVE_PATH
			}))
			.pipe(rename(id + '.html'))
			.pipe(gulp.dest('./' + parentId))
	}
});

gulp.task("sass", function () {
	gulp.src("scss/**/*scss")
		.pipe(plumber())
		.pipe(sourcemaps.init())
		.pipe(sass({
			outputStyle: 'expanded'
		}))
		.pipe(autoprefixer({browsers: ['last 3 versions', 'ie >= 10']}))
		.pipe(minifyCss({advanced: false}))
		.pipe(sourcemaps.write('./maps'))
		.pipe(gulp.dest("./css"));
});

gulp.task('watch', function () {
	gulp.watch('ejs/**/*.ejs', ['ejs']);
	gulp.watch('scss/*.scss', ['sass']);
});